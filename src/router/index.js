import { createRouter, createWebHashHistory } from 'vue-router'
const router = createRouter({
  history: createWebHashHistory(),
  routes: [
    {
      path: '/',
      name: 'home',
      redirect: '/excel'
    },
    {
      path: '/excel',
      name: 'excel',
      component: () => import('../views/ExcelView.vue')
    }
  ]
})

export default router
